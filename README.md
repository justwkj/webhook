# webhook

#### 介绍 [项目地址](https://gitee.com/justwkj/webhook)
简单的webhook集中管理

由于项目开发部署到对应环境的时候,每次手动上传比较麻烦.为了简化流程,
使用自动部署.  
以前写过简单的crontab定时任务去拉取,但是会有延迟.  
所以考虑时效性和正规性,改用webhook来通知更新代码. 

### 使用步骤
1. 需要将 hook.example.json 修改为hook.json
2. 根据实际配置 hook.json
3. 开启web服务,到当前目录(假设域名: http://webhook.justwkj.com)
4. github或gitee设置webhook地址 
http://webhook.justwkj.com?from=github&project=blogtest


### webhook部署遇到的一些问题
1. 部署脚本直接发现没有权限,需要调整如下(以www用户为例子)
    - 需要确保系统存在www用户, `cat /etc/passwd`
    - 如果用户不存在,需要添加用户
    - 用户存在,修改用户为可登陆的状态 `usermod -s /bin/bash www` 或者直接编辑 `vim /etc/passwd`
    - 需要给www用户添加公钥私钥 `ssh-keygen -t rsa -b 4096 -C "邮箱"`
    - clone项目到指定目录,并把项目权限修改 `chmod -R www.www 你的项目`
